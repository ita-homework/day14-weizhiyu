package com.oocl.afs.todolist.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oocl.afs.todolist.entity.Todo;
import com.oocl.afs.todolist.repository.TodoRepository;
import com.oocl.afs.todolist.service.dto.TodoRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TodoRepository todoRepository;

    @BeforeEach
    void setUp() {
        todoRepository.deleteAll();
    }

    @Test
    void should_find_todos() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        todo.setDone(false);
        Long id = todoRepository.save(todo).getId();

        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(id))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(todo.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(false))
        ;
    }

    @Test
    void should_create_todo() throws Exception {
        TodoRequest todoRequest = new TodoRequest();
        todoRequest.setName("Study React");

        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequestJson = objectMapper.writeValueAsString(todoRequest);

        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequestJson))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todoRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false))
        ;
    }

    @Test
    void should_find_todo_by_id() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        todo.setDone(false);
        Long id = todoRepository.save(todo).getId();
        mockMvc.perform(get("/todos/" + id))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(id))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todo.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false))
        ;
    }

    @Test
    void should_update_todo() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        todo.setDone(false);
        Long id = todoRepository.save(todo).getId();

        TodoRequest updatedTodoRequest = new TodoRequest();
        updatedTodoRequest.setDone(true);
        updatedTodoRequest.setName("Study Java");
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedTodoRequestJson = objectMapper.writeValueAsString(updatedTodoRequest);
        mockMvc.perform(put("/todos/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoRequestJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(updatedTodoRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(updatedTodoRequest.getDone()))
        ;
    }

    @Test
    void should_delete_todo() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        todo.setDone(false);
        Long id = todoRepository.save(todo).getId();

        mockMvc.perform(delete("/todos/{id}", id))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(todoRepository.findById(id).isEmpty());

    }
}