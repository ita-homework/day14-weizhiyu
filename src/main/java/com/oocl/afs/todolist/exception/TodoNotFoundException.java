package com.oocl.afs.todolist.exception;

public class TodoNotFoundException extends RuntimeException {
    public TodoNotFoundException() {
        super("Todo Id Not Found");
    }
}
