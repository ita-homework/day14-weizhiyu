package com.oocl.afs.todolist.controller;

import com.oocl.afs.todolist.entity.Todo;
import com.oocl.afs.todolist.service.TodoService;
import com.oocl.afs.todolist.service.dto.TodoRequest;
import com.oocl.afs.todolist.service.dto.TodoResponse;
import com.oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/todos")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<TodoResponse> getAllTodos() {
        List<Todo> todos = todoService.findAll();
        return todos.stream()
                .map(TodoMapper::toResponse)
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse createTodo(@RequestBody TodoRequest todoRequest) {
        Todo createdTodo = todoService.create(todoRequest);
        return TodoMapper.toResponse(createdTodo);
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable Long id) {
        Todo targetTodo = todoService.findById(id);
        return TodoMapper.toResponse(targetTodo);
    }

    @PutMapping("/{id}")
    public TodoResponse updateTodo(@PathVariable Long id, @RequestBody TodoRequest todoRequest) {
        return TodoMapper.toResponse(todoService.update(id, todoRequest));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable Long id) {
        todoService.delete(id);
    }
}
