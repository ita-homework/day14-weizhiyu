package com.oocl.afs.todolist.service;

import com.oocl.afs.todolist.entity.Todo;
import com.oocl.afs.todolist.exception.TodoNotFoundException;
import com.oocl.afs.todolist.repository.TodoRepository;
import com.oocl.afs.todolist.service.dto.TodoRequest;
import com.oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<Todo> findAll() {
        return todoRepository.findAll();
    }

    public Todo create(TodoRequest todoRequest) {
        Todo newTodo = TodoMapper.toEntity(todoRequest);
        newTodo.setDone(false);
        return todoRepository.save(newTodo);
    }

    public Todo findById(Long id) {
        return todoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
    }

    public Todo update(Long id, TodoRequest todoRequest) {
        Todo toBeUpdatedTodo = findById(id);
        if (todoRequest.getName() != null) {
            toBeUpdatedTodo.setName(todoRequest.getName());
        }
        if (todoRequest.getDone() != null) {
            toBeUpdatedTodo.setDone(todoRequest.getDone());
        }
        return todoRepository.save(toBeUpdatedTodo);
    }

    public void delete(Long id) {
        todoRepository.deleteById(id);
    }
}
