package com.oocl.afs.todolist.service.mapper;

import com.oocl.afs.todolist.entity.Todo;
import com.oocl.afs.todolist.service.dto.TodoRequest;
import com.oocl.afs.todolist.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    private TodoMapper() {}

    public static TodoResponse toResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }

    public static Todo toEntity(TodoRequest todoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest, todo);
        return todo;
    }
}
