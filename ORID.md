# O
- Front-End Concept Map: Today we draw a concept map of front-end. Through this process, we have a deeper understanding about it.

- Write the back-end logic about todo list: Yesterday we used mockAPI for backend integration. Today we wrote our own backend logic, replacing mockAPI.

- Presentation: Today, the three groups each made a presentation, the content is MVP, elevator speech, user journey and user stories respectively. After listening to the respective presentations, I have a basic understanding of these four concepts and some ideas about the connections between them.
# R

I was excited

# I

Through today's presentation I have a basic understanding of these four concepts MVP, elevator speech, user journey and user stories.

# D

I will continue to learn how to use ANTD